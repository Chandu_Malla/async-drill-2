const cards = require('./data/cards_1.json')
const lists = require('./data/lists_1.json')
const { dualFunctions } = require('./callback4.cjs')
const callback = require('./callback.cjs')

let idsSet
try {
   idsSet = Object.entries(lists).reduce((accumulator, data) => {
      for (const cardId of data[1]) {
         accumulator.push(cardId.id)
      }
      return accumulator
   }, [])
} catch (error) {
   console.error('Error in creating idsSet:', error.message)
   callback(error, null)
} finally {
   console.log('Creating idsSet is completed!')
}

let cardsSet
try {
   cardsSet = Object.entries(cards).reduce((accumulator, data) => {
      if (idsSet.includes(data[0])) {
         accumulator[data[0]] = data[1]
      }
      return accumulator
   }, {})
} catch (error) {
   console.error('Error in creating cardsSet:', error.message)
   callback(error, null)
} finally {
   console.log('Creating cardsSet is completed!')
}

const callingSixthFunction = (dualFunctions) => {
   try {
      dualFunctions()
      callback(null, cardsSet)
   } catch (error) {
      console.error('Error in callingSixthFunction:', error.message)
      callback(error, null)
   } finally {
      console.log('Calling callingSixthFunction is completed!')
   }
}

module.exports = callingSixthFunction
