const boards = require('./data/boards_1.json')
const callback = require('./callback.cjs')

const boardInformation = (boards, boardID, callback) => {
   try {
      let information = boards.reduce((accumulator, boardData) => {
         if (boardData.id === boardID) {
            accumulator = boardData
         }
         return accumulator
      }, null)
      callback(null, information)
   } catch (error) {
      callback(error, null)
   } finally {
      console.log('The boardInformation function is called!')
   }
}

module.exports = boardInformation
