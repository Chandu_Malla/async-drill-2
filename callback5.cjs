const { callingPreviousFunctions, cards, lists, listID } = require('./callback4.cjs')
const { cardsList } = require('./callback4.cjs')
const callback = require('./callback.cjs')

const callingFunctions = (callingPreviousFunctions, callback) => {
   try {
      callingPreviousFunctions('Thanos', 'Mind', callback)
      listsFunction('Space')
   } catch (error) {
      console.error('Error in callingFunctions:', error.message)
      callback(error, null)
   } finally {
      console.log('Calling callingFunctions is completed!')
   }
}

const listsFunction = (name) => {
   try {
      cardsList(cards, listID(name, lists), callback)
   } catch (error) {
      console.error('Error in listsFunction:', error.message)
      callback(error, null)
   } finally {
      console.log('Calling listsFunction is completed!')
   }
}

module.exports = callingFunctions
