const callback = (error, data) => {
   if (!error) {
      setTimeout(() => {
         console.log(data)
      }, 2000)
   } else {
      console.error('Caught', error)
   }
}

module.exports = callback
