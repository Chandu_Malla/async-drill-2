const cards = require('./data/cards_1.json')
const callback = require('./callback.cjs')

const cardsOfListID = (cards, listID, callback) => {
   try {
      let information = Object.entries(cards).reduce((accumulator, cardsList) => {
         if (cardsList[0] === listID) {
            accumulator = { [listID]: cardsList[1] }
         }
         return accumulator
      }, null)
      callback(null, information) 
   } catch (error) {
      callback(error, null)
   } finally {
      console.log('The cardsOfListID function is called!')
   }
}

module.exports = cardsOfListID
