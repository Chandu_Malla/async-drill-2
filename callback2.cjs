const lists = require('./data/lists_1.json')
const callback = require('./callback.cjs')

const listsOfBoardID = (lists, boardID, callback) => {
   try {
      let information = Object.entries(lists).reduce((accumulator, cardsList) => {
         if (cardsList[0] === boardID) {
            accumulator = { [boardID]: cardsList[1] }
         }
         return accumulator
      }, null)
      callback(null, information)
   } catch (error) {
      callback(error, null)
   } finally {
      console.log('The listsOfBoardID function is called!')
   }
}

module.exports = listsOfBoardID
