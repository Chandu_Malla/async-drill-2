const cards = require('./data/cards_1.json')
const lists = require('./data/lists_1.json')
const boards = require('./data/boards_1.json')

const boardInformation = require('./callback1.cjs')
const listsOfBoardID = require('./callback2.cjs')
const cardsOfListID = require('./callback3.cjs')
const callback = require('./callback.cjs')

const name = 'Thanos'
const cardName = 'Mind'

const idExtractor = (name) => {
   try {
      let boardID = boards.reduce((accumulator, boardData) => {
         if (boardData.name === name) {
            accumulator = boardData.id
         }
         return accumulator
      }, null)
      return boardID
   } catch (error) {
      console.error('Error in idExtractor:', error.message)
      callback(error, null)
   } finally {
      console.log('The idExtractor function is completed!')
   }
}

const listID = (name, lists) => {
   try {
      const id = Object.entries(lists).reduce((accumulator, list) => {
         for (const data of list[1]) {
            if (data.name === name) {
               accumulator = data.id
            }
         }
         return accumulator
      }, 0)
      return id
   } catch (error) {
      console.error('Error in listID:', error.message)
      callback(error, null)
   } finally {
      console.log('The listID function is completed!')
   }
}

const cardsList = cardsOfListID

const dualFunctions = () => {
   try {
      boardInformation(boards, idExtractor(name), callback)
      listsOfBoardID(lists, idExtractor(name), callback)
   } catch (error) {
      console.error('Error in dualFunctions:', error.message)
      callback(error, null)
   } finally {
      console.log('The dualFunctions function is completed!')
   }
}

const callingPreviousFunctions = (name, cardName, callback) => {
   try {
      dualFunctions()
      cardsList(cards, listID(cardName, lists), callback)
   } catch (error) {
      console.error('Error in callingPreviousFunctions:', error.message)
      callback(error, null)
   } finally {
      console.log('The callingPreviousFunctions function is completed!')
   }
}

module.exports = {
   callingPreviousFunctions,
   cardsList,
   listID,
   dualFunctions,
   cards,
   lists,
}
