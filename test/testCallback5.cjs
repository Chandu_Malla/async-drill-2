const callingFunctions = require('./../callback5.cjs')
const callback = require('./../callback.cjs')
const { callingPreviousFunctions, cards, lists, listID } = require('./../callback4.cjs')
const { cardsList } = require('./../callback4.cjs')

callingFunctions(callingPreviousFunctions, callback)
